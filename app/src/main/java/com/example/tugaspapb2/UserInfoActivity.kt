package com.example.tugaspapb2

import android.app.Activity
import android.os.Bundle
import android.widget.TextView

class UserInfoActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_info)

        val username = intent.getStringExtra("username")
        val password = intent.getStringExtra("password")

        val usernameInfoTextView = findViewById<TextView>(R.id.usernameInfoTextView)
        val passwordInfoTextView = findViewById<TextView>(R.id.passwordInfoTextView)

        usernameInfoTextView.text = "Username: $username"
        passwordInfoTextView.text = "Password: $password"
    }
}
